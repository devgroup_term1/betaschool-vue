import Vue from "vue";
import App from "./App.vue";
// import Chart from "chart.js";
import router from "./router/router";
import Buefy from "buefy";
import store from "@/store";

Vue.config.productionTip = false;

Vue.use(Buefy);

new Vue({
  router,
  store,
  render: h => h(App)
}).$mount("#app");
