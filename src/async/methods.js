import store from '../vuex'
import axios from '@/async'
import BudgefyError from '@/utils/BudgefyError'
const toggleSavingStatus = commit => () => commit('TOGGLE_SAVING_STATUS')
const addErrorToStack = commit => error => {
    if (
        !(
            error.original_error &&
            error.original_error.response &&
            error.original_error.response.status === 422
        )
    )
        return commit('ADD_ERROR_TO_STACK', error, { root: true })
}
const dispatchAsyncRequest = (
    axios_config,
    successHook,
    saveStatusToggle,
    addErrorToStack,
    failHook
) => {
    if (typeof saveStatusToggle === 'function') saveStatusToggle(true)
    axios(axios_config)
        .then(res => {
            if (typeof successHook === 'function') successHook(res)
            if (typeof saveStatusToggle === 'function') saveStatusToggle(false)
        })
        .catch(error => {
            let custom_error = null
            if (error.response) {
                const e = error.response
                custom_error = new BudgefyError({
                    title: `${e.status} - ${e.statusText}`,
                    message: e.statusText,
                    extra_details: `${e.request.responseURL}`,
                    original_error: error
                })
            } else if (error.request) {
                custom_error = new BudgefyError({
                    title: `Network error - Internet may be disconnected`,
                    message: `You've either lost internet connectivity, or there's something strange going on with the internet`,
                    extra_details: `${error.request.responseURL}`,
                    original_error: error
                })
            } else {
                custom_error = new BudgefyError({
                    title: `Unknown error`,
                    message: `We're not sure what happened here, contact us if the problem persists`,
                    extra_details: ``,
                    original_error: error
                })
            }
            if (typeof saveStatusToggle === 'function') saveStatusToggle(false)
            if (typeof addErrorToStack === 'function')
                addErrorToStack(custom_error)
            if (typeof failHook === 'function') failHook(error)
        })
}
const token_is_valid = token => {
    const token_regex = /(Bearer )([0-9a-zA-Z-_]+).([0-9a-zA-Z-_]+).([0-9a-zA-Z-_]+)/g
    return token_regex.test(token)
}

const auth_token = () => store.state.Auth.auth_token || ''

const currently_at_login = () => {
    const current_route = router.currentRoute.path || ''
    return current_route === '/login'
}

const getClaimsFromToken = token => {
    if (!token) throw new TypeError('Token cannot be null')
    const token_regex = /(?:Bearer )(?:[0-9a-zA-Z-_]+).([0-9a-zA-Z-_]+).(?:[0-9a-zA-Z-_]+)/g
    const results = token_regex.exec(token)
    const claims_base64_string = results[1] || 'e30=' //base64 {}
    const claims = JSON.parse(window.atob(claims_base64_string))
    return typeof claims === 'object' ? claims : {}
}

async function login(email, password, remember) {
    const result = await store.dispatch('attemptLogin', {
        email,
        password
    })
    if (result.authenticated) {
        store.dispatch('logAdviserIn', {
            data: result.data,
            remember
        })
        return {}
    } else {
        return result.data
    }
}

async function updateCurrentAdviserDetails() {
    const successful = await store.dispatch('updateCurrentAdviserStatus')
    if (!successful) logout()
}

const refreshToken = token => {
    store.dispatch('setAuthToken', {
        refresh: true,
        token
    })
}
const logout = () => {
    store.dispatch('setAuthToken', {
        logout: true
    })
    router.push('/login')
}

const goBackOnePage = () => {
    router.go(-1)
}

const getCookie = name => {
    let cookie_value
    if (document.cookie && document.cookie !== '') {
        const cookies = document.cookie.split(';')
        for (const cookie of cookies) {
            const current_cookie = cookie.trim()
            if (current_cookie.substring(0, name.length + 1) === name + '=') {
                cookie_value = decodeURIComponent(
                    current_cookie.substring(name.length + 1)
                )
                break
            }
        }
    }
    return cookie_value
}

const getAdviserFromCache = () => {
    return localStorage.getItem('cached_adviser')
}

const saveAdviserToCache = ({ token, adviser }) => {
    const claims = getClaimsFromToken(token)
}

export {
    toggleSavingStatus,
    addErrorToStack,
    dispatchAsyncRequest,
    token_is_valid,
    auth_token,
    getClaimsFromToken,
    currently_at_login,
    login,
    updateCurrentAdviserDetails,
    refreshToken,
    logout,
    getCookie,
    getAdviserFromCache,
    saveAdviserToCache,
    goBackOnePage
}
